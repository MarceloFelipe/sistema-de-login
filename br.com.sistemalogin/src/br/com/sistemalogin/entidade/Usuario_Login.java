package br.com.sistemalogin.entidade;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Usuario_Login {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int IdUsuario_Login;
	
	@NotNull(message = "Informe o Nome")
	private String nome;
	
	@NotNull(message = "Informe o Email")
	private String email;
	
	@NotNull(message = "Informe a Senha")
	private String senha;

	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdUsuario_Login() {
		return IdUsuario_Login;
	}

	public void setIdUsuario_Login(int idUsuario_Login) {
		IdUsuario_Login = idUsuario_Login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
