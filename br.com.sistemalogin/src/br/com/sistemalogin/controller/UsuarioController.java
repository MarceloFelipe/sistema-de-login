package br.com.sistemalogin.controller;

import br.com.sistemalogin.entidade.Usuario_Login;
import br.com.sistemalogin.dao.Usuario_LoginDAO;
import br.com.sistemalogin.util.Mensagem;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

@ManagedBean
public class UsuarioController {
	
	private Usuario_Login usuario_login;

	@EJB
	private Usuario_LoginDAO usuario_LoginDAO;
	
	public UsuarioController(){
		this.usuario_login = new Usuario_Login();
	}

	public String consultar(){
		
		 String nome = usuario_LoginDAO.todos(usuario_login.getEmail(),usuario_login.getSenha());
		
		if(nome == null){
			Mensagem.erro("Email ou Senha incorreto ");
			this.usuario_login = new Usuario_Login();
			return null;
		}
		else{	
			return "sistema.xhtml";
		}
	}
	
	public Usuario_Login getUsuario_login() {
		return usuario_login;
	}

	public void setUsuario_login(Usuario_Login usuario_login) {
		this.usuario_login = usuario_login;
	}
}
