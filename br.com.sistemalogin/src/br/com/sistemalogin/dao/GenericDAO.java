package br.com.sistemalogin.dao;

import javax.persistence.*;

import br.com.sistemalogin.controller.*;

public class GenericDAO<T> {
	
	@PersistenceContext
	protected EntityManager em;
	
	private UsuarioController usuarioController;
	
	private Class<T> classe;
	
	public GenericDAO(Class<T> classe){
		this.classe = classe;
	}
	
	public String salvar(T entidade){
		try{
			em.merge(entidade);
		}catch(Exception ex){
			return "Ocorreu o erro "+ex.getMessage();
		}
		
		return null;
	}

}
